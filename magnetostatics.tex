\chapter{Magnetostatics}
This chapter concerns with solutions of the equations
\begin{align}
\nabla \cdot \mathbf{B} &= 0,\label{divB0}\\
\nabla \times \mathbf{B} &= \mu_0 \mathbf{J}.\label{ampere}
\end{align}
From Amp\`{e}re's law, equation \ref{ampere}, and Stokes's theorem
\begin{equation}
\int_L \mathbf{B} \cdot d\mathbf{l}=\mu_0\int_S \mathbf{J}\cdot d\mathbf{a},
\end{equation}
in which the right-hand side is the current enclosed by the boundary $L$.

\section{Biot-Savart law and magnetic field}
Under the approximation of steady current 
\begin{equation}
\frac{\partial\mathbf{J}}{\partial t}=0,    
\end{equation}
the magnetic field is given by the Biot-Savart law (Figure \ref{fig:biotsavart})
\begin{equation}
\begin{aligned}
\mathbf{B}(\mathbf{r})&=\frac{\mu_0}{4\pi}\int_L \mathbf{I}\times\frac{\mathbf{r}-\mathbf{r}'}{|\mathbf{r}-\mathbf{r}'|^3}dl',\\
&=\frac{\mu_0}{4\pi}\int_S \mathbf{K}(\mathbf{r}')\times\frac{\mathbf{r}-\mathbf{r}'}{|\mathbf{r}-\mathbf{r}'|^3}da',\\
&=\frac{\mu_0}{4\pi}\int_V \mathbf{J}(\mathbf{r}')\times\frac{\mathbf{r}-\mathbf{r}'}{|\mathbf{r}-\mathbf{r}'|^3}da',
\end{aligned}
\end{equation}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{biotsavart}
\caption{Illustration of Biot-Savart law.}
\label{fig:biotsavart}
\end{figure}
\newline
\textbf{Example 1}: Magnetic field of a wire segment (Figure \ref{fig:wiresegment})\newline
\begin{equation}
\mathbf{B}=\frac{\mu_0 I}{4\pi s}(\sin{\theta_2}-\sin{\theta_1})\hat{\bm{\phi}}.
\end{equation}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{wiresegment}
\caption{A wire segment.}
\label{fig:wiresegment}
\end{figure}
\newline
\textbf{Example 2}: Magnetic field of a long wire\newline
\begin{equation}
\mathbf{B}=\frac{\mu_0 I}{2\pi s}\hat{\bm{\phi}}.
\end{equation}
\newline
\textbf{Example 3}: Magnetic field on the axis of a circular loop with radius $R$ (Figure \ref{fig:loop})\newline
\begin{equation}
\mathbf{B}=\frac{\mu_0 I}{2}\frac{R^2}{\left(R^2+z^2\right)^{\frac{3}{2}}}\hat{\mathbf{z}}.
\end{equation}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{loop}
\caption{A circular loop.}
\label{fig:loop}
\end{figure}
\newline
\textbf{Example 4}: Magnetic field on the axis of a solenoid with $n$ turns per unit length (Figure \ref{fig:solenoid})\newline
\begin{equation}
\mathbf{B}=-\frac{\mu_0 nI}{2}\int_{\theta_1}^{\theta_2}|\sin{\theta}|\hat{\mathbf{z}}.
\end{equation}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{solenoid}
\caption{A solenoid.}
\label{fig:solenoid}
\end{figure}
\newline
\textbf{Example 5}: Magnetic field of a infinite solenoid with $n$ turns per unit length (Figure \ref{fig:solenoid2})\newline
\begin{equation}
\mathbf{B}=\begin{cases}
\mu_0nI\hat{\mathbf{z}} \quad\mbox{inside},\\
0 \quad\mbox{outside}.
\end{cases}
\end{equation}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{solenoid2}
\caption{An infinite solenoid.}
\label{fig:solenoid2}
\end{figure}

\section{Magnetic vector potential}
The magnetic vector potential is defined as
\begin{align}
\nabla \times \mathbf{A}=\mathbf{B},\label{magpotential}\\
\nabla \cdot \mathbf{A}=0,\label{coulombgauge}
\end{align}
in which is the second equation is referred to as Coulomb gauge. Now Amp\`{e}re's law, equation \ref{ampere}, becomes
\begin{equation}
    \nabla^2\mathbf{A}=-\mu_0\mathbf{J}.
\end{equation}
\par
From equation \ref{magpotential} and Stokes's theorem
\begin{equation}
\int_L\mathbf{A}\cdot d\mathbf{l}=\int_S\mathbf{B}\cdot d\mathbf{a},
\end{equation}
in which is right-hand side is the magnetic flux through the surface enclosed by $L$.
\par
If $\mathbf{J}$ goes to zero at infinity
\begin{equation}
\begin{aligned}
\mathbf{A}(\mathbf{r})&=\frac{\mu_0}{4\pi}\int_L \frac{\mathbf{I}}{|\mathbf{r}-\mathbf{r}'|}dl',\\
&=\frac{\mu_0}{4\pi}\int_S \frac{\mathbf{K}(\mathbf{r}')}{|\mathbf{r}-\mathbf{r}'|}da',\\
&=\frac{\mu_0}{4\pi}\int_V \frac{\mathbf{J}(\mathbf{r}')}{|\mathbf{r}-\mathbf{r}'|}d\tau'.
\end{aligned}
\end{equation}
\newline
\textbf{Example 1}: Magnetic vector potential of a infinite solenoid with $n$ turns per unit length\newline
\begin{equation}
\mathbf{A}=\begin{cases}
\frac{\mu_0nI}{2}s\hat{\bm{\phi}} \quad s \leq R,\\
\frac{\mu_0nI}{2} \frac{R^2}{s}\hat{\bm{\phi}} \quad s \geq R
\end{cases}
\end{equation}

\section{Boundary conditions}
The magnetic field is discontinuous at a surface current $\mathbf{K}$
\begin{gather}
B^{\perp}_{\mbox{above}}=B^{\perp}_{\mbox{below}}
,\label{bcBnormal}\\
\left|\mathbf{B}^{\parallel}_{\mbox{above}}-\mathbf{B}^{\parallel}_{\mbox{below}}\right|=\mu_0K
,\label{bcBnorm}
\end{gather}
which can be combined as
\begin{equation}
\mathbf{B}_{\mbox{above}}-\mathbf{B}_{\mbox{below}}=\mu_0\mathbf{K}\times \hat{\mathbf{n}}.
\end{equation}
The magnetic vector potential is continuous but its' normal derivative is not
\begin{gather}
\mathbf{A}_{\mbox{above}}=\mathbf{A}_{\mbox{below}}
,\label{bcA}\\
\frac{\partial \mathbf{A}_{\mbox{above}}}{\partial \hat{\mathbf{n}}}-\frac{\partial \mathbf{A}_{\mbox{below}}}{\partial \hat{\mathbf{n}}}=-\mu_0\mathbf{K}.
\label{bcdA}
\end{gather}

\section{Worked examples}
\subsection{A circular current loop}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{loop1}
\caption{A circular current loop.}
\label{fig:loop1}
\end{figure}
\begin{equation}
\mathbf{J}(\mathbf{r}')=I\delta(z')\delta\left(\sqrt{x'^2+y'^2}-R\right)\frac{-y'\hat{\mathbf{x}}'+x'\hat{\mathbf{y}}'}{\sqrt{x'^2+y'^2}},
\end{equation}
in which, the delta functions are to restrict the current to a circular loop of radius $R$ on the xy-plane and the vector on the right-hand side is exactly $\hat{\bm{\phi}}'$. 
\begin{equation}
\begin{aligned}
\mathbf{A}(\mathbf{r})&=\frac{\mu_0I}{4\pi}\int_V \delta(z')\delta\left(\sqrt{x'^2+y'^2}-R\right) \frac{-y'\hat{\mathbf{x}}'+x'\hat{\mathbf{y}}'}{\sqrt{(x-x')^2+(y-y')^2+(z-z')^2}\sqrt{x'^2+y'^2}} dx'dy'dz',\\
&=\frac{\mu_0IR}{4\pi}\int_{-\pi}^{\pi}\frac{-\sin\phi'\hat{\mathbf{x}}'+\cos\phi'\hat{\mathbf{y}}'}{\sqrt{(x-R\cos\phi')^2+(y-R\sin\phi')^2+z^2}}d\phi',
\end{aligned}
\end{equation}
where we have made a change of variables 
\begin{equation}
x'=s'\cos\phi',y'=s'\sin\phi',z'=z',dx'dy'dz'=s'ds'd\phi'dz',
\end{equation}
and taken advantage of $\int \delta(x-t)f(x)dx=f(t)$. 
\par
Define $F(x,y,R,\phi)=(x-R\cos\phi)^2+(y-R\sin\phi)^2$ and $a=\frac{\mu_0IR}{4\pi}$
\begin{equation}
\begin{aligned}
A_x&=-a\int_{-\pi}^{\pi}\frac{\sin\phi'}{\sqrt{F(x,y,R,\phi')+z^2}}d\phi',\\
A_y&=a\int_{-\pi}^{\pi}\frac{\cos\phi'}{\sqrt{F(x,y,R,\phi')+z^2}}d\phi',\\
A_z&=0,
\end{aligned} 
\end{equation}
\par
In cylindrical coordinates, with $G(s,R,\phi)=s^2+R^2-2Rs\cos\phi$,
\begin{equation}
x=s\cos\phi,y=s\sin\phi,z=z,
\end{equation}
and
\begin{equation}
\begin{aligned}
\hat{\mathbf{x}}&=\cos\phi \hat{\mathbf{s}}-\sin\phi \hat{\bm{\phi}},\\
\hat{\mathbf{y}}&=\sin\phi \hat{\mathbf{s}}+\cos\phi \hat{\bm{\phi}},\\
\hat{\mathbf{z}}&=\hat{\mathbf{z}},
\end{aligned} 
\end{equation}
the magnetic vector potential is
\begin{equation}
\begin{aligned}
A_s&=A_x\cos\phi+A_y\sin\phi,\\
&=a\int_{-\pi}^{\pi}\frac{\sin(\phi-\phi')}{\sqrt{G(s,R,\phi-\phi')+z^2}}d\phi',\\
&=a\int_{-\pi}^{\pi}\frac{\sin\phi'}{\sqrt{G(s,R,\phi')+z^2}}d\phi'=0,\\
A_{\phi}&=-A_x\sin\phi+A_y\cos\phi,\\
&=a\int_{-\pi}^{\pi}\frac{\cos(\phi-\phi')}{\sqrt{G(s,R,\phi-\phi')+z^2}}d\phi',\\
&=a\int_{-\pi}^{\pi}\frac{\cos\phi'}{\sqrt{G(s,R,\phi')+z^2}}d\phi',\\
A_z&=0,
\end{aligned} 
\end{equation}
of which $A_s=0$ because it is integration of an odd function over its whole period. $A_{\phi}=A_{\phi}(s,z)$ also for the same reasons.
\par
The magnetic field can be computed from $\mathbf{B}=\nabla\times\mathbf{A}$
\begin{equation}
\begin{aligned}
B_x&=-\frac{\partial A_y}{\partial z} =az\int_{-\pi}^{\pi}\frac{\cos\phi'}{\sqrt{F(x,y,R,\phi')+z^2}^3}d\phi',\\
B_y&=\frac{\partial A_x}{\partial z} =az\int_{-\pi}^{\pi}\frac{\sin\phi'}{\sqrt{F(x,y,R,\phi')+z^2}^3}d\phi',\\
B_z&=\frac{\partial A_y}{\partial x}-\frac{\partial A_x}{\partial y},\\
&=a\int_{-\pi}^{\pi}\frac{R-x\cos\phi'-y\sin\phi'}{\sqrt{F(x,y,R,\phi')+z^2}^3}d\phi',
\end{aligned}
\end{equation}
or in cylindrical coordinates
\begin{equation}
\begin{aligned}
B_s&=az\int_{-\pi}^{\pi}\frac{\cos\phi'}{\sqrt{G(s,R,\phi')+z^2}^3}d\phi',\\
B_{\phi}&=-az\int_{-\pi}^{\pi}\frac{\sin\phi'}{\sqrt{G(s,R,\phi')+z^2}^3}d\phi'=0,\\
B_z&=a\int_{-\pi}^{\pi}\frac{R-s\cos\phi'}{\sqrt{G(s,R,\phi')+z^2}^3}d\phi',
\end{aligned} 
\end{equation}
where $B_\phi=0$ by a similar argument as above. Notice that along the z-axis, $x=0,y=0,z=z$, the magnetic field reduces to $\mathbf{B}=\frac{\mu_0 I}{2}\frac{R^2}{\left(R^2+z^2\right)^{\frac{3}{2}}}\hat{\mathbf{z}}$ as shown previously.

\subsection{A solenoid}
For a solenoid of radius $R$ height $2h$ with $n$ turns per unit length (Figure \ref{fig:solenoid2}), $\mathbf{K}=nI\hat{\bm{\phi}}$ and the normal direction is $\hat{\mathbf{n}}=\hat{\mathbf{s}}$, only the normal derivative of $A_{\phi}$ is discontinuous
\begin{equation}
 \frac{\partial A_{\phi}}{\partial s}\biggr\rvert_{s=R^+}-\frac{\partial A_{\phi}}{\partial s}\biggr\rvert_{s=R^-}=-\mu_0nI.   
\end{equation}
Using $\int\frac{dt}{\sqrt{t^2+a^2}}=\ln|t+\sqrt{t^2+a^2}|$, the magnetic vector potential is 
\begin{equation}
\begin{aligned}
A_x&=-na\int_{-h}^h\int_{-\pi}^{\pi}\frac{\sin\phi'}{\sqrt{F(x,y,R,\phi')+(z-z')^2}}d\phi'dz',\\
&=-na\int_{-\pi}^{\pi}\sin\phi'\ln\left| \frac{z+h+\sqrt{F(x,y,R,\phi')+(z+h)^2}}{z-h+\sqrt{F(x,y,R,\phi')+(z-h)^2}}\right|d\phi',\\
A_y&=na\int_{-h}^h\int_{-\pi}^{\pi}\frac{\cos\phi'}{\sqrt{F(x,y,R,\phi')+(z-z')^2}}d\phi'dz',\\
&=na\int_{-\pi}^{\pi}\cos\phi'\ln\left| \frac{z+h+\sqrt{F(x,y,R,\phi')+(z+h)^2}}{z-h+\sqrt{F(x,y,R,\phi')+(z-h)^2}}\right|d\phi',\\
A_z&=0,
\end{aligned} 
\end{equation}
or in cylindrical coordinates
\begin{equation}
\begin{aligned}
A_s&=A_z=0,\\
A_{\phi}&=na\int_{-h}^h\int_{-\pi}^{\pi}\frac{\cos\phi'}{\sqrt{G(s,R,\phi')+(z-z')^2}}d\phi'dz',\\
&=na\int_{-\pi}^{\pi}\cos\phi'\ln\left| \frac{z+h+\sqrt{G(s,R,\phi')+(z+h)^2}}{z-h+\sqrt{G(s,R,\phi')+(z-h)^2}}\right|d\phi',
\end{aligned} 
\end{equation}
\par
Using $\int\frac{tdt}{\sqrt{t^2+a^2}^3}=-\frac{1}{\sqrt{t^2+a^2}}$ and $\int\frac{dt}{\sqrt{t^2+a^2}^3}=\frac{t^2}{a^2\sqrt{t^2+a^2}}$, the magnetic field is
\begin{equation}
\begin{aligned}
B_x&=na\int_{-h}^h\int_{-\pi}^{\pi}\frac{(z-z')\cos\phi'}{\sqrt{F(x,y,R,\phi')+(z-z')^2}^3}d\phi'dz',\\
&=na\int_{-\pi}^{\pi}\left(\frac{\cos\phi'}{\sqrt{F(x,y,R,\phi')+(z-h)^2}}-\frac{\cos\phi'}{\sqrt{F(x,y,R,\phi')+(z+h)^2}}\right)d\phi',\\
B_y&=na\int_{-h}^h\int_{-\pi}^{\pi}\frac{(z-z')\sin\phi'}{\sqrt{F(x,y,R,\phi')+(z-z')^2}^3}d\phi'dz',\\
&=na\int_{-\pi}^{\pi}\left(\frac{\sin\phi'}{\sqrt{F(x,y,R,\phi')+(z-h)^2}}-\frac{\sin\phi'}{\sqrt{F(x,y,R,\phi')+(z+h)^2}}\right)d\phi',\\
B_z&=na\int_{-h}^h\int_{-\pi}^{\pi}\frac{R-x\cos\phi'-y\sin\phi'}{\sqrt{F(x,y,R,\phi')+(z-z')^2}^3}d\phi'dz',\\
&=na\int_{-\pi}^{\pi}\frac{R-x\cos\phi'-y\sin\phi'}{F(x,y,R,\phi')}\left(\frac{(z+h)^2}{\sqrt{F(x,y,R,\phi')+(z+h)^2}}-\frac{(z-h)^2}{\sqrt{F(x,y,R,\phi')+(z-h)^2}}\right)d\phi',
\end{aligned}
\end{equation}
or in cylindrical coordinates
\begin{equation}
\begin{aligned}
B_s&=na\int_{-h}^h\int_{-\pi}^{\pi}\frac{(z-z')\cos\phi'}{\sqrt{G(s,R,\phi')+(z-z')^2}^3}d\phi'dz',\\
&=na\int_{-\pi}^{\pi}\left(\frac{\cos\phi'}{\sqrt{G(s,R,\phi')+(z-h)^2}}-\frac{\cos\phi'}{\sqrt{G(s,R,\phi')+(z+h)^2}}\right)d\phi',\\
B_{\phi}&=0,\\
B_z&=na\int_{-h}^h\int_{-\pi}^{\pi}\frac{R-s\cos\phi'}{\sqrt{G(s,R,\phi')+(z-z')^2}^3}d\phi'dz',\\
&=na\int_{-\pi}^{\pi}\frac{R-s\cos\phi'}{G(s,R,\phi')}\left(\frac{(z+h)^2}{\sqrt{G(s,R,\phi')+(z+h)^2}}-\frac{(z-h)^2}{\sqrt{G(s,R,\phi')+(z-h)^2}}\right)d\phi',
\end{aligned} 
\end{equation}

\section{Finite difference methods for Poisson's equation}
This section presents solutions to Poisson's equations
\begin{equation}
    \nabla^2\mathbf{A}=-\mu_0\mathbf{J},
    \label{poissonA}
\end{equation}
by finite difference methods. We consider here only a special case 
\begin{equation}
\mathbf{J}=(0,J_{\phi}(s,z),0),
\end{equation}
under which the two previous worked examples of a circular current loop and a solenoid fall. Because of the rotational symmetry of the current, the magnetic vector potential also display similar symmetry
\begin{equation}
\mathbf{A}=(0,A_{\phi}(s,z),0).
\end{equation}
In cylindrical coordinates, equation \ref{poissonA} becomes
\begin{equation}
\frac{1}{s}\frac{\partial }{\partial s}\left(s\frac{\partial A_{\phi}(s,z)}{\partial s}\right)+\frac{\partial^2 A_{\phi}(s,z)}{\partial^2z}-\frac{A_{\phi}(s,z)}{s^2}=-\mu_0J_{\phi}(s,z),\quad s>0,z\in\mathcal{R},
\end{equation}
or equivalently
\begin{equation}
\frac{\partial^2 A_{\phi}(s,z)}{\partial s^2}+\frac{1}{s}\frac{\partial A_{\phi}(s,z)}{\partial s}+\frac{\partial^2 A_{\phi}(s,z)}{\partial^2z}-\frac{A_{\phi}(s,z)}{s^2}=-\mu_0J_{\phi}(s,z),\quad s>0,z\in\mathcal{R}.
\label{poissonAcylin}
\end{equation}
As boundary conditions, the magentic vector potential vanishes at $s=0$, because of symmetry, and far way from the localized current
\begin{equation}
A_{\phi}(0,z)=\lim_{s\to\infty}A_{\phi}(s,z)=\lim_{z\to\pm\infty}A_{\phi}(s,z)=0.
\end{equation}
\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{fddomain}
\caption{Finite difference discretization.}
\label{fig:fddomain}
\end{figure}
For finite difference methods, choose a domain
\begin{equation}
0\le s\le S_M,\quad -Z_M\le z\le Z_M,
\end{equation}
such that the boundaries $S_M$ and $Z_M$ are far from the current, and discretize it by $n+2$ points in each direction (Figure \ref{fig:fddomain})
\begin{equation}
\triangle s=\frac{S_M}{n+1},\quad \triangle z=\frac{2Z_M}{n+1},
\end{equation}
and denote
\begin{equation}
A_{\phi}(i,j)=A_{\phi}(i\triangle s,Z_M-j\triangle z),\quad J_{\phi}(i,j)=J_{\phi}(i\triangle s,Z_M-j\triangle z),\quad i,j\in\{0,1,...,n+1\},
\end{equation}
Using center finite differences, equation \ref{poissonAcylin} is satisfied by
\begin{equation}
\begin{split}
&\frac{A_{\phi}(i+1,j)-2A_{\phi}(i,j)+A_{\phi}(i-1,j)}{\triangle s^2}+\frac{1}{i\triangle s}\frac{A_{\phi}(i+1,j)-A_{\phi}(i-1,j)}{\triangle s}\\
+&\frac{A_{\phi}(i,j+1)-2A_{\phi}(i,j)+A_{\phi}(i,j-1)}{\triangle z^2}-\frac{A_{\phi}(i,j)}{(i\triangle s)^2}=-\mu_0J_{\phi}(i,j),\quad i,j\in\{1,2,...,n\},
\end{split}
\label{poissonAfd}
\end{equation}
or
\begin{equation}
a_1(i)A_{\phi}(i+1,j)+a_0(i)A_{\phi}(i,j)+a_{-1}(i)A_{\phi}(i-1,j)+bA_{\phi}(i,j+1)+bA_{\phi}(i,j-1)=c(i,j),
\label{poissonAfd1}
\end{equation}
with
\begin{equation}
a_1(i)=\frac{i+1}{i\triangle s^2},\quad a_0(i)=-\frac{2i^2+1}{(i\triangle s)^2}-\frac{2}{\triangle z^2},\quad a_{-1}(i)=\frac{i-1}{i\triangle s^2},\quad b=\frac{1}{\triangle z^2},\quad c(i,j)=-\mu_0J_{\phi}(i,j).
\end{equation}
The boundary conditions are
\begin{equation}
A_{\phi}(0,j)=A_{\phi}(n+1,j)=A_{\phi}(i,0)=A_{\phi}(i,n+1)=0,\quad i,j\in\{0,1,...,n+1\}.
\end{equation}
Equations \ref{poissonAfd1} can be written in maxtrix form as
\begin{equation}
Mx=c,
\end{equation}
the unknown $x\in\mathcal{R}^{n^2}$
\begin{equation}
x=\begin{bmatrix}
A_{\phi}(1,1) &... & A_{\phi}(n,1) & A_{\phi}(1,2) &...& A_{\phi}(1,2) &...& A_{\phi}(1,n) & A_{\phi}(1,n)  
\end{bmatrix}^T,
\end{equation}
the right-hand side $c\in\mathcal{R}^{n^2}$
\begin{equation}
c=\begin{bmatrix}
c(1,1) &... & c(n,1) & c(1,2) &...& c(1,2) &...& c(1,n) & c(1,n)  
\end{bmatrix}^T,
\end{equation}
and $M$ is a $n^2\times n^2$ block tridiagonal matrix
\begin{equation}
M=\begin{bmatrix}
P & bI &   &   &   & \\
bI & P & bI &   &   & \\
  & . & . & . &   & \\
  &   & . & . & . & \\
  &   &   &   bI & P & bI\\
  &   &   &     & bI & P
\end{bmatrix},
\end{equation}
with $P,I\in\mathcal{R}^{n\times n}$
\begin{equation}
P=\begin{bmatrix}
a_0(1) & a_1(1) &   &   &   & \\
a_{-1}(2) & a_0(2) & a_1(2) &   &   & \\
  & . & . & . &   & \\
  &   & . & . & . & \\
  &   &   &   a_{-1}(n-1) & a_0(n-1) & a_1(n-1)\\
  &   &   &     & a_{-1}(n)& a_0(n)
\end{bmatrix},
\end{equation}
